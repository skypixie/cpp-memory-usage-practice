﻿#include <iostream>
#include <string>


class Player
{
private:
    std::string name;
    int score;

public:
    Player(std::string newName="?", int newScore=0) : name(newName), score(newScore)
    {}

    std::string getName()
    {
        return name;
    }

    int getScore()
    {
        return score;
    }

    void printPlayer() {
        std::cout << name << '\t' << score << '\n';
    }
};


void sortPlayers(Player *arr, int n) {
    bool isUnsorted;
    do {
        isUnsorted = false;
        for (int i = 0; i < (n - 1); i++) {
            if (arr[i].getScore() < arr[i + 1].getScore()) {
                isUnsorted = true;
                for (; i < (n - 1); i++) {
                    if (arr[i].getScore() < arr[i + 1].getScore()) {
                        std::swap(arr[i], arr[i + 1]);
                    }
                }
            }
        }
    } while (isUnsorted);
}


int main()
{
    int playerPoolSize;
    std::cout << "How many people are playing?\n";
    std::cin >> playerPoolSize;

    // создаем и заполняем массив с игроками
    Player* players = new Player[playerPoolSize];
    for (int i = 0; i < playerPoolSize; ++i) {
        std::string name;
        int score;
        std::cin >> name >> score;

        players[i] = Player(name, score);
    }

    sortPlayers(players, playerPoolSize);

    // выводим результат сортировки на экран
    std::cout << "SCORE TABLE\n";
    for (int i = 0; i < playerPoolSize; ++i) {
        players[i].printPlayer();
    }

    delete [] players;
}